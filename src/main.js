import Vue from 'vue'
import App from './App.vue'
import axios from "axios";
import router from './router'
import store from './store'
import "./utils/nprogress.css";
import vuetify from './plugins/vuetify'
import Toasted from "vue-toasted";
import moment from "moment";
import InfiniteLoading from 'vue-infinite-loading';

Vue.config.productionTip = false
Vue.use(InfiniteLoading, { /* options */ });
Vue.use(axios);
Vue.use(Toasted, {
  iconPack: "fontawesome",
});
Vue.prototype.moment = moment;
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
