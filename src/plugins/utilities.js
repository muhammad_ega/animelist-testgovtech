export default ({ store }, inject) => {
  inject('utilities', {
    async dialog(data) {
      await store.dispatch('utilities/initDialog', data);
    },
    async snackbar(data) {
      await store.dispatch('utilities/initSnackbar', data);
    },
  });
}
