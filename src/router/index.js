import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/HomeView.vue";
import NProgress from "nprogress";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        redirect: "/home/anime",
    },
    {
        path: "/home/:type",
        name: "Home-View",
        component: Home,
    },
    {
        path: "/home/:type/:id",
        name: "DetailView",
        component: () => import("../views/DetailView.vue"),
        props: true,
    },
    {
        path: "/favorite",
        name: "FavoriteView",
        component: () => import("../views/FavoriteView.vue"),
        props: true,
    },
];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes,
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
});

router.beforeResolve((to, from, next) => {
    // If this isn't an initial page load.
    if (to.name) {
        // Start the route progress bar.
        NProgress.start();
    }
    next();
});

router.afterEach(() => {
    // Complete the animation of the route progress bar.
    NProgress.done();
});

export default router;
